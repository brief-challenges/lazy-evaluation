"use strict";

let Lazy = class Lazy {
  constructor() {
    this.queue=[];

    this.add = function(){
      let args = Array.from(arguments);
      let newFunction = args[0];

      // Any additional arguments should be applied to the function
      if (args.length>1) {
        newFunction = newFunction.bind.apply(newFunction, args);
      }

      // Add the function to the queue to be evaluated
      this.queue.push(newFunction);
      return this;
    };

    this.evaluate = function(target){
      while (this.queue.length) {

        // Get the functions one by one, in the order they were added
        let functionToApply = this.queue.shift();
        target = target.map(function(n){
          return functionToApply.apply(functionToApply, [n]);
        });
      }
      return target;
    };
  }
}
module.exports = Lazy;
