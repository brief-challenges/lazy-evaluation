"use strict";

let Lazy = require('../Lazy');
var expect = require('expect');

describe('Lazy', function() {
  describe('#add', function() {
    describe('timesTwo(a)', function(){

      let lazy = new Lazy();
      let result = lazy.add(function timesTwo(a) { return a*2; });
      let addedFunction = lazy.queue[0];

      it('timesTwo(2) => 4', function() {
        expect(addedFunction.apply(addedFunction, [2])).toEqual(4);
      });
      it('timesTwo(5) => 10', function() {
        expect(addedFunction.apply(addedFunction, [5])).toEqual(10);
      });
      it('timesTwo(15) => 30', function() {
        expect(addedFunction.apply(addedFunction, [15])).toEqual(30);
      });
    });

    describe('multiply(3,b)', function(){

      let lazy = new Lazy();
      let result = lazy
        .add(function multiply(a,b) { return a*b; },3);
      let addedFunction = lazy.queue[0];

      it('multiply(3, 2) => 6', function() {
        expect(addedFunction.apply( addedFunction,[2])).toEqual(6);
      });
      it('multiply(3, 3) => 9', function() {
        expect(addedFunction.apply( addedFunction,[3])).toEqual(9);
      });
      it('multiply(3, 10) => 30', function() {
        expect(addedFunction.apply( addedFunction,[10])).toEqual(30);
      });
    });
    describe('multiply(5,b)', function(){
      let lazy = new Lazy();
      let result = lazy
        .add(function multiply(a,b) { return a*b; },5);
      let addedFunction = lazy.queue[0];

      it('multiply(5, 2) => 10', function() {
        expect(addedFunction.apply( addedFunction,[2])).toEqual(10);

      });
      it('multiply(5, 5) => 25', function() {
        expect(addedFunction.apply( addedFunction,[5])).toEqual(25);
      });
      it('multiply(5, 30) => 150', function() {
        expect(addedFunction.apply( addedFunction,[30])).toEqual(150);
      });
    });

  });
  describe('#evalute', function() {
    let lazy = new Lazy();

    describe('no add, evaluate: [1,2,3]', function() {
      let result = lazy
        .evaluate([1,2,3]);
      it('=> [1,2,3]', function() {
        expect(result).toEqual([1,2,3]);
      });
    });
    describe('add: [ timesTwo(a)], evaluate: [1,2,3]', function() {
      let result = lazy
        .add(function timesTwo(a) { return a*2; })
        .evaluate([1,2,3]);
      it('=> [2,4,6]', function() {
        expect(result).toEqual([2,4,6]);
      });
    });
    describe('add: [ timesTwo(a), plus(1, b) ], evaluate: [1,2,3]', function() {
      let result = lazy
        .add(function timesTwo(a) { return a * 2; })   // simple function
        .add(function plus(a, b) { return a + b; }, 1) // a plus function that will be given 1 as its first argument
        .evaluate([1, 2, 3]);
      it('=> [3,5,7]', function() {
        expect(result).toEqual([3,5,7]);
      });
    });
    describe('add: [ timesTwo(a), timesTwo(a), plus(2+b) ], evaluate: [1,2,3]', function() {
      let result = lazy
        .add(function timesTwo(a) { return a * 2; })
        .add(function timesTwo(a) { return a * 2; })
        .add(function plus(a, b) { return a + b; }, 2)
        .evaluate([1, 2, 3]);
      it('=> [6,10,14]', function() {
        expect(result).toEqual([6,10,14]);

      });
    });
    describe('add: [ timesTwo(a), plus(5+b) ], evaluate: [1,2,3]', function() {
      let result = lazy
        .add(function timesTwo(a) { return a * 2; })
        .add(function plus(a, b) { return a + b; }, 5)
        .evaluate([3, 6, 5]);
      it('=> [11, 17, 15]', function() {
        expect(result).toEqual([11, 17, 15]);
      });
    });
    describe('add: [ Math.sqrt ], evaluate: [2,3,4]', function() {
      let result = lazy
        .add(Math.sqrt)
        .evaluate([4,9,16]);
      it('=> [2,3,4]', function() {
        expect(result).toEqual([2,3,4]);
      });
    });
  });
});
